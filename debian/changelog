node-i18next-xhr-backend (3.2.2+ds-10) unstable; urgency=medium

  * d/control: remove node-babel-polyfill builddep (Closes: #980623)
  * d/control,d/copyright,d/changelog: update uploader e-mail

 -- Nicolas Mora <babelouest@debian.org>  Wed, 20 Jan 2021 16:26:26 -0500

node-i18next-xhr-backend (3.2.2+ds-9) unstable; urgency=medium

  * Team upload.
  * Remove deprecated options for rollup-plugin-node-resolve 11
  * Drop deprecated rollup-plugin-babel usage

 -- Pirate Praveen <praveen@debian.org>  Fri, 01 Jan 2021 21:01:28 +0530

node-i18next-xhr-backend (3.2.2+ds-8) unstable; urgency=medium

  * Team upload.
  * Use node-rollup-plugin-terser build dependency (now available in the
    archive). Also use system path to resolve dependencies.
  * Drop legacy node-resolve plugin usage
  * Bump Standards-Version to 4.5.1 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Thu, 31 Dec 2020 17:55:00 +0530

node-i18next-xhr-backend (3.2.2+ds-7) unstable; urgency=medium

  * Team upload.

  * fix build-depend explicitly on recent
    node-babel-core node-babel-plugin-transform-runtime
    node-babel-preset-env node-babel-polyfill;
    depend on recent node-babel-runtime
    (not node-babel7);
    stop depend on nodejs

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 18 Dec 2020 18:40:36 +0100

node-i18next-xhr-backend (3.2.2+ds-6) UNRELEASED; urgency=low

  * Set upstream metadata fields: Bug-Submit.

 -- Debian Janitor <janitor@jelmer.uk>  Fri, 28 Aug 2020 20:13:04 -0000

node-i18next-xhr-backend (3.2.2+ds-5) unstable; urgency=medium

  * Fix link between node-i18next-xhr-backend and libjs-i18next-xhr-backend
  * d/control: set debhelper-compat version to 13

 -- Nicolas Mora <nicolas@babelouest.org>  Mon, 29 Jun 2020 10:12:03 -0400

node-i18next-xhr-backend (3.2.2+ds-4) unstable; urgency=medium

  * Team upload
  * Add Testsuite: autopkgtest-pkg-nodejs
  * Update dependency on node-babel-runtime to node-babel7
  * Bump Standards-Version to 4.5.0 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Wed, 13 May 2020 16:29:11 +0530

node-i18next-xhr-backend (3.2.2+ds-3) unstable; urgency=medium

  * Team upload
  * Build with babel 7

 -- Pirate Praveen <praveen@debian.org>  Sun, 03 May 2020 10:06:34 +0530

node-i18next-xhr-backend (3.2.2+ds-2) unstable; urgency=medium

  * Team upload
  * Reupload to unstable

 -- Xavier Guimard <yadd@debian.org>  Fri, 10 Jan 2020 06:20:05 +0100

node-i18next-xhr-backend (3.2.2+ds-1) unstable; urgency=medium

  * Team upload
  * Exclude minified files from import
  * New upstream version 3.2.2+ds

 -- Xavier Guimard <yadd@debian.org>  Sat, 28 Dec 2019 09:29:42 +0100

node-i18next-xhr-backend (3.2.0-1) unstable; urgency=low

  * Initial release (Closes: #913204)

 -- Nicolas Mora <nicolas@babelouest.org>  Sun, 01 Dec 2019 09:43:41 -0500
